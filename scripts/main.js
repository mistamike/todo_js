$(function () {
    var inputSection = $('.main-section__input'); // Main input section
    var todoList = $('#todo-list'); // Unsorted list of elements
    var itemId = 0; // Initial element ID
    var finishedCounter = 0; // Counter of finished to-do elements
    var unfinishedCounter = 0; // Counter of unfinished to-do elements
    var allElementsArr = []; // Array of to-do elements
    var showArr = []; // Array to display elements per page
    var elementsPerPage = 5; // Elements per page
    var currentPage = 1; // Current page on start
    var numOfPages = 1; // Quantity of pages on start
    var isCall = true; // Value for blur 'event'

    //#region Call of functions

    // Removing list element
    todoList.on('click', '.clear-todo', removeTodo);

    // Clear all list elements
    $('.clear-all').on('click', clearAll);

    // Toggling class 'done' if checkbox is checked/unchecked
    todoList.on('click', '.checkElement', doneUndone);

    // Choose/unchoose all elements
    $('.choose-all').on('click', checkAllElements);

    // Check radiobuttons
    $('.radioButton').on('click',radioChecked);

    // Add new list element by click and clear input area
    $('#main-section__form').on('submit', function (e) {
        e.preventDefault();
        if (inputSection.val() === "") { // The case when nothing entered
            return;
        } else {
            // Add new element
            todoList.append(
                '<li class="list-element">'
                + '<input class="checkElement" type="checkbox">'
                + '<div class = "list-element__content">'
                + inputSection.val()
                + '</div>'
                + '<button class="clear-todo" type="button">X</button>'
                + '</li>'
            );}
        // Add id for new element
        $('.list-element').last().attr('id', 'id' + itemId); // Add id for new element
        allElementsArr.push({id: 'id' + itemId, text: inputSection.val(), done: false}); // Add element in 'allElementsArr' array
        itemId++;
        // $('.list-element').addClass('undone');
        unfinishedCounter++; // Gain unfinished elements counter
        inputSection.val(''); // Clear input section
        updateCountersView();
        showHideElements();
        showDeleteFinishedButton();

    });

    // Edit single element by double click
    todoList.on('dblclick', '.list-element__content', editElement);

    // Apply edited element (by double click)
    todoList.on('keydown','.edit', applyEditedElement);

    // Toggle class 'currentPage'
    $('#navigation').on('click', '.page', selectCurrentPage);

    // Cancel editing element
    todoList.on('blur', '.edit', function(){
        var inputVal = $('.edit');
        isCall ? Blurhandler(inputVal) : isCall = false;
        isCall = true;
    });

    // Delete all finished elements
    $('.actionButtons').on('click', '.delete-finished', deleteAllFinished);

    //#endregion Call of functions


    //#region Functions description
    // Removing list element function
    function removeTodo() {
        var id = $(this).parent().attr('id');
        var index = findIndexById(id);
        if ($(this).parent().hasClass('done')) { // Decrease counter for done/undone element
            finishedCounter--;
        } else {
            unfinishedCounter--;
        }
        $(this).parent().remove();
        allElementsArr.splice(index, 1); // Deleting element from array
        updateCountersView();
        showHideElements();
    }


    //Clear all list elements function
    function clearAll() {
        var checkAll = $('.choose-all');
        finishedCounter = 0;
        unfinishedCounter = 0;
        $('.list-element').remove();
        allElementsArr = [];
        checkAll.removeClass("unchoose-all");
        checkAll.text('Finish all');
        updateCountersView();
        showHideElements();
    }


    //Toggling class 'done' if checkbox is checked/unchecked function
    function doneUndone() {
        var id = $(this).parent().attr('id');
        var index = findIndexById(id);
        $(this).parent().toggleClass('done');
        if ($(this).parent().hasClass('done')){
            finishedCounter++;
            unfinishedCounter--;
            allElementsArr[index].done = true;
        } else {
            finishedCounter--;
            unfinishedCounter++;
            allElementsArr[index].done = false;
        }
        showDeleteFinishedButton();
        updateCountersView();
        showHideElements();
    }


    // Finish/cancel all elements function
    function checkAllElements() {
        var checkAll = $('.choose-all');
        var isChecked = !checkAll.hasClass('unchoose-all');
        var checkElement = $('.checkElement');
        if (isChecked) {
            finishedCounter = finishedCounter + unfinishedCounter;
            unfinishedCounter = 0;
            checkElement.attr('checked');
            $('.list-element').addClass('done');
            toggleArrStatus(isChecked); // Toggle status (done/undone) for 'Finish all' button
        } else {
            unfinishedCounter = unfinishedCounter + finishedCounter;
            finishedCounter = 0;
            checkElement.removeAttr('checked');
            $('.list-element').removeClass('done');
            toggleArrStatus(isChecked); // Toggle status (done/undone) for 'Cancel all' button
        }
        checkElement.prop('checked', isChecked);
        checkAll.toggleClass("unchoose-all");
        checkAll.text(isChecked ? 'Cancel all' : 'Finish all'); // Rename button
        updateCountersView();
        showHideElements();
    }


    //Count done/undone elements function
    function updateCountersView() {
        $('.not-finished').text('Not finished: ' + unfinishedCounter);
        $('.finished').text('Finished: ' + finishedCounter);
    }

    //Check radiobuttons function
    function radioChecked() {
        $('.radioButton').change(function(){
            $('.radioButton').removeClass('checkedButton');
            $(this).addClass('checkedButton');
            showHideElements();
        });
    }

    //Show and hide elements function (all/active/completed)
    function showHideElements() {
        if ($('#radio1').hasClass('checkedButton')) { // 'All' chosen
            renderPage();
        } else if ($('#radio2').hasClass('checkedButton')) { // 'Active' chosen
            renderPage({done: false})
        } else if ($('#radio3').hasClass('checkedButton')) { // 'Completed' chosen
            renderPage({done: true})
        } else {
            renderPage();
        }
    }

    // Find index of array by id function
    function findIndexById(id) {
        var index = null;
        allElementsArr.forEach(function(el, idx) {
            if(el.id === id) {
                index = idx;
            }
        });
        return index
    }

    // Toggle status (done/undone) for 'Finish all' button function
    function toggleArrStatus(isChecked) {
        for (var i = 0;  i < allElementsArr.length; i++) {
            allElementsArr[i].done = isChecked;
        }
    }

    // Edit single element by double click function
    function editElement() {
        var editableContent = $(this);
        var textArea = $('<input class="edit" type="text">');
        editableContent.replaceWith(textArea); // Replace element text with input area
        textArea.val(editableContent.text()).focus(); // Focus on created input
    }

    // Apply edited element (by double click) function
    function applyEditedElement(e) {
        var edit = $('.edit');
        var id = $(this).parent().attr('id');
        var index = findIndexById(id); // Find element by ID
        var originalText =  allElementsArr[index].text;
        // Accept changes by "Enter" button
        if (e.keyCode === 13) {
            if (edit.val() === "") {
                return;
            } else {
                isCall = false;
                $(this).replaceWith($('<div class = "list-element__content">' + edit.val() + '</div>'));
                allElementsArr[index].text = edit.val();
            }
            // Cancel changes by "Esc" button
        } else if (e.keyCode === 27) {
            isCall = false;
            $(this).replaceWith($('<div class = "list-element__content">' + originalText + '</div>'));
        }

    }

    // Recount pages per ALL list elements function
    function recountPages(renderArr) {
        var numPages;
        numPages = Math.ceil(renderArr.length / elementsPerPage);
        return numPages;
    }

    // Render pages function
    function renderPage(renderFlag) {
        var renderArr = []; // New array for rendering pages
        if(!renderFlag) {
            renderArr = allElementsArr;
        }
        else {
            renderArr = allElementsArr.filter(function (el) {
                return el.done === renderFlag.done
            });
        }
        var numOfPagesNew = recountPages(renderArr);
        $('.page').remove();
        // Save first page if we have before deleting one element
        // or in case of total removing
        if ($('.list-element').length === 0) {
            $('#navigation').append('<a href="#" class="page" id="' + 'pageId' + 1 + '">' + 1 + '</a>');
            if (i === currentPage && numOfPagesNew === numOfPages) {
                $('.page').last().addClass('currentPage');
            }
        }
        for (var i = 1; i <= numOfPagesNew; i++)
        {
            $('#navigation').append('<a href="#" class="page" id="' + 'pageId' + i + '">' + i + '</a>');
            if (i === currentPage && numOfPagesNew === numOfPages) {
                $('.page').last().addClass('currentPage');
            }
        }
        if(numOfPagesNew !== numOfPages) {
            currentPage = numOfPagesNew;
            numOfPages = numOfPagesNew;
            $('.page').last().addClass('currentPage');
        }
        $('.main-section__input').focus();
        var startRender = (currentPage - 1) * elementsPerPage; // Starting position with which the first element for the current page will start
        var endRender = startRender + elementsPerPage; // The final position at which the countdown for the current page will end
        showArr = renderArr.slice(startRender, endRender); // Add in new array "showArr" elements which match their pages
        $('.list-element').hide();
        renderFromArray(showArr);
    }

    // Toggle class 'currentPage' function
    function selectCurrentPage(e) {
        e.preventDefault();
        var renderArr = []; // New array for rendering pages
        // Render pages according to the buttons ("All"/"Active/"Completed")
        if ($('#radio1').hasClass('checkedButton')) {
            renderArr = allElementsArr;
        } else if ($('#radio2').hasClass('checkedButton')) {
            renderArr = allElementsArr.filter(function (el) {
                return !el.done
            });
        } else if ($('#radio3').hasClass('checkedButton')) {
            renderArr = allElementsArr.filter(function (el) {
                return el.done
            });
        } else {
            renderArr = allElementsArr;
        }
        $('.page').removeClass('currentPage'); // Remove 'currentPage' status from all pages
        $(this).addClass('currentPage'); // Add 'currentPage' status to chosen page
        currentPage = +($(this).attr('id').replace('pageId', ''));
        $('.main-section__input').focus();
        var startRender = (currentPage - 1) * elementsPerPage; // Starting position with which the first element for the current page will start
        var endRender = startRender + elementsPerPage; // The final position at which the countdown for the current page will end
        showArr = renderArr.slice(startRender, endRender); // Add in new array "showArr" elements which match their pages
        $('.list-element').hide();
        renderFromArray(showArr);
    }

    // Show pages from "showArr" array function
    function renderFromArray(list) {
        list.forEach(function(el, idx) {
            $('#'+ el.id).show();
        })
    }

    // Show/hide "delete all finished" button function
    function showDeleteFinishedButton() {
        if ($('.done').length) {
            $('.delete-finished').show();
        } else {
            $('.delete-finished').hide();
        }
    }

    // 'Blur' event handler function
    function Blurhandler(inputVal) {
        var id = inputVal.parent().attr('id');
        var index = findIndexById(id);
        var originalText =  allElementsArr[index].text;
        inputVal.replaceWith($('<div class = "list-element__content">' + originalText + '</div>'));
    }

    // Delete all finished elements function
    function deleteAllFinished() {
        $('.done').remove();
        var undoneArr = allElementsArr.filter(function(el){
            return !el.done;
        });
        allElementsArr = undoneArr;
        showDeleteFinishedButton();
        finishedCounter = 0;
        updateCountersView();
        showHideElements();
    }
    //#endregion Functions description
});